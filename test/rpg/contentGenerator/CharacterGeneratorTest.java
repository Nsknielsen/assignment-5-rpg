package rpg.contentGenerator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import rpg.character.RpgCharacter;

import static org.junit.jupiter.api.Assertions.*;

class CharacterGeneratorTest {

    private CharacterGenerator characterGenerator;

    @BeforeEach
    void setUp() {
        characterGenerator = new CharacterGenerator();
    }

    @Test
    public void CreateCharacter_ValidInfo_Pass(){
        RpgCharacter newCharacter = characterGenerator.createCharacter("testman", "ra");
        assertEquals("testman", newCharacter.name());
    }

}
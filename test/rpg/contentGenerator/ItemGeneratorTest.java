package rpg.contentGenerator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import rpg.constants.ArmorType;
import rpg.constants.Slot;
import rpg.constants.WeaponType;
import rpg.item.Armor;
import rpg.item.Weapon;

import static org.junit.jupiter.api.Assertions.*;

class ItemGeneratorTest {

    private ItemGenerator itemGenerator;

    @BeforeEach
    void setUp() {
        itemGenerator = new ItemGenerator();
    }

    @Test
    public void CreateStarterWeapon_Pass(){
        Weapon axe = itemGenerator.createStarterWeapon("ax");
        assertEquals(WeaponType.AXE, axe.type());
    }

    @Test public void CreateStarterHelmet_Pass(){
        Armor helmet = itemGenerator.createStarterArmor("le", "he");
        assertEquals("cow-based helmet", helmet.name());
    }
}
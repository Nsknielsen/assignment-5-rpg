package rpg.integration.mage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import rpg.attributes.BasePrimaryAttributes;
import rpg.character.Mage;
import rpg.constants.ArmorType;
import rpg.constants.Slot;
import rpg.exception.InvalidArmorException;
import rpg.item.Armor;

import static org.junit.jupiter.api.Assertions.*;

public class ArmorMageIntegrationTest {
    private Mage testMage;
    private Armor testArmor;

    @BeforeEach
    public void setUp(){
        testMage = new Mage();
        testArmor = new Armor();
    }

    @Test public void CanEquipLevel1Hood_Pass () throws InvalidArmorException {
        testArmor.setSlot(Slot.HEAD);
        testArmor.setType(ArmorType.CLOTH);
        testArmor.setName("cool hood");
        testArmor.setLevel(1);
        testArmor.setPrimaryAttributes(new BasePrimaryAttributes(0, 0, 2));
        assertTrue(testMage.equipArmor(testArmor));
    }

    @Test public void CannotEquipLevel2Hood_InvalidArmorException() {
        testArmor.setSlot(Slot.HEAD);
        testArmor.setType(ArmorType.CLOTH);
        testArmor.setLevel(2);
        Exception exception = assertThrows(InvalidArmorException.class,
                () -> testMage.equipArmor(testArmor));
        assertEquals("Too high item level or invalid armor type to equip", exception.getMessage());
    }

    @Test public void CannotEquipPlateArmor_InvalidArmorException(){
        testArmor.setSlot(Slot.BODY);
        testArmor.setType(ArmorType.PLATE);
        testArmor.setLevel(1);
        Exception exception = assertThrows(InvalidArmorException.class,
                () -> testMage.equipArmor(testArmor));
        assertEquals("Too high item level or invalid armor type to equip", exception.getMessage());
    }

    @Test public void CanEquipSecondHeadArmor_Pass() throws InvalidArmorException {
        testArmor.setSlot(Slot.HEAD);
        testArmor.setType(ArmorType.CLOTH);
        testArmor.setLevel(1);
        testArmor.setName("cool cap");
        testArmor.setPrimaryAttributes(new BasePrimaryAttributes(0, 0, 2));
        testMage.equipArmor(testArmor);
        Armor newArmor = new Armor();
        newArmor.setSlot(Slot.HEAD);
        newArmor.setLevel(1);
        newArmor.setType(ArmorType.CLOTH);
        newArmor.setPrimaryAttributes(new BasePrimaryAttributes(8, 0, 2));
        newArmor.setName("extra cool cap");
        testMage.equipArmor(newArmor);
        assertEquals("extra cool cap", testMage.allArmor().get(Slot.HEAD).name());
    }

    @Test public void ArmorAdds4ToTotalIntel() throws InvalidArmorException{
        testArmor.setPrimaryAttributes(new BasePrimaryAttributes(0, 2, 4));
        testArmor.setSlot(Slot.HEAD);
        testArmor.setName("cool hood");
        testArmor.setLevel(1);
        testArmor.setType(ArmorType.CLOTH);
        testMage.equipArmor(testArmor);
        assertEquals(12, testMage.totalMainAttr());
    }

    @Test public void Adding2ArmorAdds7ToTotalIntel() throws InvalidArmorException{
        testArmor.setName("that hat");
        testArmor.setLevel(1);
        testArmor.setSlot(Slot.HEAD);
        testArmor.setType(ArmorType.CLOTH);
        testArmor.setPrimaryAttributes(new BasePrimaryAttributes(0, 0, 4));
        testMage.equipArmor(testArmor);
        Armor newArmor = new Armor();
        newArmor.setName("kevlar");
        newArmor.setLevel(1);
        newArmor.setSlot(Slot.BODY);
        newArmor.setType(ArmorType.CLOTH);
        newArmor.setPrimaryAttributes(new BasePrimaryAttributes(0, 0, 3));
        testMage.equipArmor(newArmor);
        assertEquals(15, testMage.totalPrimaryAttributes().intel());
    }

    @Test public void DpsWithArmorIs112_Pass() throws InvalidArmorException{
        testArmor.setName("that hat");
        testArmor.setLevel(1);
        testArmor.setSlot(Slot.HEAD);
        testArmor.setType(ArmorType.CLOTH);
        testArmor.setPrimaryAttributes(new BasePrimaryAttributes(0, 0, 4));
        testMage.equipArmor(testArmor);
        assertEquals(1.12, testMage.calculateCharacterDps());
    }
}

package rpg.integration.mage;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import rpg.character.Mage;
import rpg.constants.Slot;
import rpg.constants.WeaponType;
import rpg.exception.InvalidWeaponException;
import rpg.item.Weapon;

import static org.junit.jupiter.api.Assertions.*;

public class PrinterIntegrationTest {

    @Test public void PrinterWorks() throws InvalidWeaponException {
        Mage testMage = new Mage();
        testMage.setName("testman");
        testMage.levelUp();
        Weapon testWeapon = new Weapon();
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setType(WeaponType.STAFF);
        testWeapon.setLevel(1);
        testWeapon.setDamage(8);
        testWeapon.setAttackSpeed(1.1);
        testMage.equipWeapon(testWeapon);
        System.out.println(testMage.toString());
    }
}

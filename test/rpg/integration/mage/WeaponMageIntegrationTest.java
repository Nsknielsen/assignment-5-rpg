package rpg.integration.mage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import rpg.attributes.BasePrimaryAttributes;
import rpg.character.Mage;
import rpg.constants.ArmorType;
import rpg.constants.Slot;
import rpg.constants.WeaponType;
import rpg.exception.InvalidArmorException;
import rpg.exception.InvalidWeaponException;
import rpg.item.Armor;
import rpg.item.Weapon;

import static org.junit.jupiter.api.Assertions.*;

public class WeaponMageIntegrationTest {

    private Mage testMage;
    private Weapon testWeapon;

    @BeforeEach
    void setUp(){
        testMage = new Mage();
        testWeapon = new Weapon();
    }

    @Test public void CanEquipLevel1Wand_Pass () throws InvalidWeaponException{
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setType(WeaponType.WAND);
        testWeapon.setLevel(1);
        assertTrue(testMage.equipWeapon(testWeapon));
    }

    @Test public void CannotEquipLevel2Wand_InvalidWeaponException(){
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setType(WeaponType.WAND);
        testWeapon.setLevel(2);
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> testMage.equipWeapon(testWeapon));
        assertEquals("Too high item level or invalid weapon type to equip", exception.getMessage());
    }

    @Test public void CannotEquipAxe_InvalidWeaponException(){
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setType(WeaponType.AXE);
        testWeapon.setLevel(1);
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> testMage.equipWeapon(testWeapon));
        assertEquals("Too high item level or invalid weapon type to equip", exception.getMessage());
    }

    @Test public void CanEquipSecondWeapon_Pass() throws InvalidWeaponException{
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setType(WeaponType.STAFF);
        testWeapon.setLevel(1);
        testWeapon.setName("saxophone");
        testMage.equipWeapon(testWeapon);
        Weapon newWeapon = new Weapon();
        newWeapon.setSlot(Slot.WEAPON);
        newWeapon.setLevel(1);
        newWeapon.setType(WeaponType.WAND);
        newWeapon.setName("feather");
        testMage.equipWeapon(newWeapon);
        assertEquals("feather", testMage.weapon().get(Slot.WEAPON).name());
    }

    @Test public void BaseDpsIs108_pass(){
        assertEquals(1.08, testMage.calculateCharacterDps());
    }

    @Test public void DpsWithWeaponIs944_Pass() throws InvalidWeaponException{
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setType(WeaponType.STAFF);
        testWeapon.setLevel(1);
        testWeapon.setDamage(8);
        testWeapon.setAttackSpeed(1.1);
        testMage.equipWeapon(testWeapon);
        assertEquals(9.5, testMage.calculateCharacterDps());
    }

    @Test public void DpsWithWeaponAndArmorIs985() throws InvalidWeaponException, InvalidArmorException {
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setType(WeaponType.STAFF);
        testWeapon.setLevel(1);
        testWeapon.setDamage(8);
        testWeapon.setAttackSpeed(1.1);
        testMage.equipWeapon(testWeapon);
        Armor testArmor = new Armor();
        testArmor.setName("that hat");
        testArmor.setLevel(1);
        testArmor.setSlot(Slot.HEAD);
        testArmor.setType(ArmorType.CLOTH);
        testArmor.setPrimaryAttributes(new BasePrimaryAttributes(0, 0, 4));
        testMage.equipArmor(testArmor);
        assertEquals(9.86, testMage.calculateCharacterDps());
    }

}

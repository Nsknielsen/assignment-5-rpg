package rpg.item;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import rpg.constants.Slot;
import rpg.constants.WeaponType;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {
    private Weapon testWeapon;

    @BeforeEach
    void setUp() {
        testWeapon = new Weapon();
    }

    @Test
    public void GetsName_Pass(){
        testWeapon.setName("big axe");
        assertEquals("big axe", testWeapon.name());
    }

    @Test public void GetsRequiredLevel_Pass(){
        testWeapon.setLevel(3);
        assertEquals(3, testWeapon.level());
    }

    @Test public void GetsSlot_Pass(){
        testWeapon.setSlot(Slot.WEAPON);
        assertEquals(Slot.WEAPON, testWeapon.slot());
    }

    @Test public void GetsDamage_Pass(){
        testWeapon.setDamage(12);
        assertEquals(12, testWeapon.damage());
    }

    @Test public void GetsAtkSpeed_Pass(){
        testWeapon.setAttackSpeed(2.3);
        assertEquals(2.3, testWeapon.attackSpeed());
    }

    @Test public void GetsType_Pass(){
        testWeapon.setType(WeaponType.HAMMER);
        assertEquals(WeaponType.HAMMER, testWeapon.type());
    }

    @Test public void GetsDps(){
        testWeapon.setDamage(10);
        testWeapon.setAttackSpeed(0.8);
        assertEquals(8, testWeapon.calculateWeaponDps());
    }






}
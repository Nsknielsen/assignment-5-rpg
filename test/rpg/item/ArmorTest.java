package rpg.item;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import rpg.attributes.BasePrimaryAttributes;
import rpg.constants.ArmorType;
import rpg.constants.Slot;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {

    private Armor testArmor;
    
    @BeforeEach
    void setUp() {
        testArmor = new Armor();
    }

    @Test
    public void GetsName_Pass(){
        testArmor.setName("tin hat");
        assertEquals("tin hat", testArmor.name());
    }

    @Test public void GetsRequiredLevel_Pass(){
        testArmor.setLevel(8);
        assertEquals(8, testArmor.level());
    }

    @Test public void GetsSlot_Pass(){
        testArmor.setSlot(Slot.HEAD);
        assertEquals(Slot.HEAD, testArmor.slot());
    }

    @Test public void GetsType_Pass(){
        testArmor.setType(ArmorType.PLATE);
        assertEquals(ArmorType.PLATE, testArmor.type());
    }

    @Test public void GetsBasePrimaryAttrs_Pass(){
        testArmor.setPrimaryAttributes(new BasePrimaryAttributes(0, 1, 4));
        assertEquals(4, testArmor.primaryAttributes().intel());
    }


}
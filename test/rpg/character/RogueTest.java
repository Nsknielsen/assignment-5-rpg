package rpg.character;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {

    private RpgCharacter testRogue;

    @BeforeEach
    void setUp() {
        testRogue = new Rogue();
    }

    @Test
    public void Has6Dex_Pass (){
        assert (testRogue.basePrimaryAttributes().dex() == 6);
    }

    @Test public void SetName_Pass(){
        testRogue.setName("testman");
        assert (testRogue.name().equals("testman"));
    }

    @Test public void LevelUp_Pass(){
        testRogue.levelUp();
        assertEquals(2, testRogue.level());
    }

    @Test public void DexIsBasePrimary_Pass(){
        assertEquals(6, testRogue.baseMainAttr());
    }

    @Test public void TotalMainAttrStartsAt7_Pass(){
        assertEquals(6, testRogue.totalMainAttr());
    }

    @Test public void LevelUpIncreasesStatsEverywhere_Pass(){
        testRogue.levelUp();
        //asserts for intel, baseprimary, total primary
        assertEquals(10, testRogue.basePrimaryAttributes().dex());
    }

}
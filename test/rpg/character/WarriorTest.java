package rpg.character;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {

    private RpgCharacter testWarrior;

    @BeforeEach
    void setUp() {
        testWarrior = new Warrior();
    }

    @Test
    public void Has5str_Pass (){
        assert (testWarrior.basePrimaryAttributes().str() == 5);
    }

    @Test public void SetName_Pass(){
        testWarrior.setName("testman");
        assert (testWarrior.name().equals("testman"));
    }

    @Test public void LevelUp_Pass(){
        testWarrior.levelUp();
        assertEquals(2, testWarrior.level());
    }

    @Test public void StrIsBasePrimary_Pass(){
        assertEquals(5, testWarrior.baseMainAttr());
    }

    @Test public void TotalMainAttrStartsAt5_Pass(){
        assertEquals(5, testWarrior.totalMainAttr());
    }

    @Test public void LevelUpIncreasesStatsEverywhere_Pass(){
        testWarrior.levelUp();
        //asserts for intel, baseprimary, total primary
        assertEquals(8, testWarrior.basePrimaryAttributes().str());
        assertEquals(8, testWarrior.baseMainAttr());
    }

}
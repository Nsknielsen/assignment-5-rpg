package rpg.character;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {

    private Mage testMage;

    @BeforeEach
    void setUp() {
        testMage = new Mage();
    }

    @Test public void Has8Intel_Pass (){
        assert (testMage.basePrimaryAttributes().intel() == 8);
    }

    @Test public void SetName_Pass(){
        testMage.setName("testman");
        assert (testMage.name().equals("testman"));
    }

    @Test public void LevelUp_Pass(){
        testMage.levelUp();
        assertEquals(2, testMage.level());
    }

    @Test public void IntelIsBasePrimary_Pass(){
        assertEquals(8, testMage.baseMainAttr());
    }

    @Test public void TotalMainAttrStartsAt8_Pass(){
        assertEquals(8, testMage.totalMainAttr());
    }

    @Test public void LevelUpIncreasesStatsEverywhere_Pass(){
        testMage.levelUp();
        //asserts for intel, baseprimary, total primary
        assertEquals(13, testMage.basePrimaryAttributes().intel());
    }


}
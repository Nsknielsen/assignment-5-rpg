package rpg.character;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {

    private RpgCharacter testRanger;

    @BeforeEach
    void setUp() {
        testRanger = new Ranger();
    }

    @Test
    public void Has7Dex_Pass (){
        assert (testRanger.basePrimaryAttributes().dex() == 7);
    }

    @Test public void SetName_Pass(){
        testRanger.setName("testman");
        assert (testRanger.name().equals("testman"));
    }

    @Test public void LevelUp_Pass(){
        testRanger.levelUp();
        assertEquals(2, testRanger.level());
    }

    @Test public void DexIsBasePrimary_Pass(){
        assertEquals(7, testRanger.baseMainAttr());
    }

    @Test public void TotalMainAttrStartsAt7_Pass(){
        assertEquals(7, testRanger.totalMainAttr());
    }

    @Test public void LevelUpIncreasesStatsEverywhere_Pass(){
        testRanger.levelUp();
        //asserts for intel, baseprimary, total primary
        assertEquals(12, testRanger.basePrimaryAttributes().dex());
    }

}
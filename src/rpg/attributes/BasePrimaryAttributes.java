package rpg.attributes;

public class BasePrimaryAttributes extends PrimaryAttributes{

    public BasePrimaryAttributes(int str, int dex, int intel){
        super(str, dex, intel);
    }
    public BasePrimaryAttributes(){}

    @Override
    public void updateAttributes(BasePrimaryAttributes newAttributes) {
        this.str += newAttributes.str();
        this.dex += newAttributes.dex();
        this.intel += newAttributes.intel();
    }

    public void levelUpAttributes(BasePrimaryAttributes extraAttributes){

    }

}

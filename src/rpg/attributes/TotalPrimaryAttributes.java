package rpg.attributes;

public class TotalPrimaryAttributes extends PrimaryAttributes{

    public TotalPrimaryAttributes(int str, int dex, int intel){
        super(str, dex, intel);
    }

    @Override
    public void updateAttributes(BasePrimaryAttributes newAttributes) {
        this.str = newAttributes.str();
        this.dex = newAttributes.dex();
        this.intel = newAttributes.intel();
    }
}

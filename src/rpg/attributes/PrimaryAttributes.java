package rpg.attributes;

public abstract class PrimaryAttributes {
    //char stats
    protected  int str;
    protected int dex;
    protected int intel;


    //empty constructor for use in armor's baseprimattr
    public PrimaryAttributes(){}

    //constructor with args for creating rpg characters
    public PrimaryAttributes(int str, int dex, int intel){
        this.str = str;
        this.dex = dex;
        this.intel = intel;
    }

    //increase one or more attributes
    public abstract void updateAttributes(BasePrimaryAttributes newAttributes);

    //str set and get
    public void setStr(int str) {
        this.str = str;
    }

    public int str(){
        return this.str;
    }

    //dex set and get
    public void setDex(int dex) {
        this.dex = dex;
    }

    public int dex(){
        return this.dex;
    }

    //intel set and get
    public void setIntel(int intel) {
        this.intel = intel;
    }

    public int intel(){
        return this.intel;
    }
}

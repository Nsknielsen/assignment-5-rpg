package rpg.contentGenerator;

import rpg.character.*;

public class CharacterGenerator {

    //create a character with user-defined name and class
    public RpgCharacter createCharacter(String name, String characterClass){
        RpgCharacter character = null;
        switch (characterClass.toLowerCase()){
            case "ma" -> character = new Mage();
            case "ra" -> character = new Ranger();
            case "ro" -> character = new Rogue();
            case "wa" -> character = new Warrior();
        }
        character.setName(name);
        return character;
    }

}

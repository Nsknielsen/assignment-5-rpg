package rpg.contentGenerator;

import rpg.attributes.BasePrimaryAttributes;
import rpg.constants.ArmorType;
import rpg.constants.Slot;
import rpg.constants.WeaponType;
import rpg.item.Armor;
import rpg.item.Weapon;


public class ItemGenerator {

    //creates a weapon with base stats
    public Weapon createStarterWeapon(String weaponType){
        Weapon weapon = new Weapon();
        switch (weaponType.toLowerCase()){
            case "wa": weapon.setName("drumstick"); weapon.setType(WeaponType.WAND); break;
            case "st": weapon.setName("garden variety pole"); weapon.setType(WeaponType.STAFF); break;
            case "ha" : weapon.setName("bonk"); weapon.setType(WeaponType.HAMMER);break;
            case "ax" : weapon.setName("and my axe"); weapon.setType(WeaponType.AXE);break;
            case "sw" : weapon.setName("very royal sword"); weapon.setType(WeaponType.SWORD);break;
            case "bo" : weapon.setName("pewpew"); weapon.setType(WeaponType.BOW);break;
            case "da" : weapon.setName("sneaky stuff"); weapon.setType(WeaponType.DAGGER);break;
        }
        weapon.setSlot(Slot.WEAPON);
        weapon.setLevel(1);
        weapon.setDamage(8);
        weapon.setAttackSpeed(1.2);
        return weapon;
    }

    //creates armor with base stats
    public Armor createStarterArmor(String armorType, String slot) {
        Armor armor = new Armor();
        armor.setLevel(1);
        String armorName = "";
        switch (armorType.toLowerCase()) {
            case "cl":
                armorName += "cheap ";
                armor.setType(ArmorType.CLOTH);
                break;
            case "le":
                armorName += "cow-based ";
                armor.setType(ArmorType.LEATHER);
                break;
            case "ma":
                armorName += "ring-based ";
                armor.setType(ArmorType.MAIL);
                break;
            case "pl":
                armorName += "turtle-style ";
                armor.setType(ArmorType.PLATE);
                break;
        }
        switch (slot.toLowerCase()) {
            case "he":
                armorName += "helmet";
                armor.setSlot(Slot.HEAD);
                break;
            case "bo":
                armorName += "body armor";
                armor.setSlot(Slot.BODY);
                break;
            case "le":
                armorName += "trousers";
                armor.setSlot(Slot.LEGS);
                break;
        }
        armor.setName(armorName);
        armor.setPrimaryAttributes(new BasePrimaryAttributes(2, 2, 2));
        return armor;
    }}

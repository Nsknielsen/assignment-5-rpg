package rpg.constants;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}

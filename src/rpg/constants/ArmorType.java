package rpg.constants;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}

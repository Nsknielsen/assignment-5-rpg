package rpg.printer;

import rpg.character.RpgCharacter;

public class Printer {

    //print details about a character
    public String printCharacter(RpgCharacter rpgCharacter){
        StringBuilder builder = new StringBuilder();
        builder.append("Name: " + rpgCharacter.name() + "\n");
        builder.append("Level: " + rpgCharacter.level() + "\n");
        builder.append("Str: " + rpgCharacter.basePrimaryAttributes().str() + "\n");
        builder.append("Dex: " + rpgCharacter.basePrimaryAttributes().dex() + "\n");
        builder.append("Intel: " + rpgCharacter.basePrimaryAttributes().intel() + "\n");
        builder.append("DPS: " + rpgCharacter.calculateCharacterDps() + "\n");
        return builder.toString();
    }

}

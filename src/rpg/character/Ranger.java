package rpg.character;

import rpg.attributes.BasePrimaryAttributes;
import rpg.constants.ArmorType;
import rpg.constants.WeaponType;

public class Ranger extends RpgCharacter{

    public Ranger(){
        super(1, 7, 1);
        this.allowedWeapons.add(WeaponType.BOW);
        this.allowedArmor.add(ArmorType.LEATHER);
        this.allowedArmor.add(ArmorType.MAIL);
    }

    @Override
    public int baseMainAttr(){
        return this.basePrimaryAttributes.dex();
    }

    @Override
    public int totalMainAttr(){return this.totalPrimaryAttributes.dex();}

    @Override
    public void levelUp(){
        super.levelUp(new BasePrimaryAttributes(1, 5, 1));
        this.totalPrimaryAttributes.updateAttributes(new BasePrimaryAttributes(1, 5, 1));
    }
}

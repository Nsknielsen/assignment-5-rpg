package rpg.character;

import rpg.attributes.BasePrimaryAttributes;
import rpg.attributes.TotalPrimaryAttributes;
import rpg.constants.ArmorType;
import rpg.constants.Slot;
import rpg.constants.WeaponType;
import rpg.exception.InvalidArmorException;
import rpg.exception.InvalidWeaponException;
import rpg.item.Armor;
import rpg.item.Weapon;
import rpg.printer.Printer;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class RpgCharacter {

    //char stats
    protected BasePrimaryAttributes basePrimaryAttributes;
    protected TotalPrimaryAttributes totalPrimaryAttributes;

    protected int level;
    protected String name;

    protected HashMap<Slot, Armor> allArmor;
    protected HashMap<Slot, Weapon> weapon;

    protected ArrayList<WeaponType> allowedWeapons;
    protected ArrayList<ArmorType> allowedArmor;

    protected Printer printer;

    //create with stats, rules for allowed equipment, and associated printer object
    public RpgCharacter(int str, int dex, int intel) {
        this.basePrimaryAttributes = new BasePrimaryAttributes(str, dex, intel);
        this.totalPrimaryAttributes = new TotalPrimaryAttributes(str, dex, intel);
        this.level = 1;
        this.allArmor = new HashMap<>();
        this.weapon = new HashMap<>();
        this.allowedWeapons = new ArrayList<>();
        this.allowedArmor = new ArrayList<>();
        this.printer = new Printer();
    }

    //calculate char dps based on weapon and armor (if any)
    public double calculateCharacterDps(){
        double attrModifier = 1 + (this.totalMainAttr()/100.0);
        double weaponDps;
        if (this.weapon.get(Slot.WEAPON) != null){
            weaponDps = this.weapon.get(Slot.WEAPON).calculateWeaponDps();
        }
        else {
            return 1 * attrModifier;
        }
        Double characterDps = weaponDps * attrModifier;
        return roundOffDps(characterDps);
    }

    //helper method: round calculated dps down to 2 decimals
    private double roundOffDps(Double characterDps){
        characterDps = characterDps*100;
        characterDps = (double) Math.round(characterDps);
        characterDps = characterDps/100;
        return characterDps;
    }

    //get total primary attrs
    public TotalPrimaryAttributes totalPrimaryAttributes(){
        return this.totalPrimaryAttributes;
    }

    //update total primary attrs when levelling up
    public void updateTotalPrimaryAttrs(BasePrimaryAttributes extraAttributes){
        this.totalPrimaryAttributes.updateAttributes(extraAttributes);
    }

    //override-  update total primary attrs when adding/replaciung equipment up
    public void updateTotalPrimaryAttrs(){
        BasePrimaryAttributes newTotalPrimaryAttributes = new BasePrimaryAttributes();
        newTotalPrimaryAttributes.updateAttributes(this.basePrimaryAttributes);
        for (Armor armor: this.allArmor.values()){
            newTotalPrimaryAttributes.updateAttributes(armor.primaryAttributes());
        }
        this.totalPrimaryAttributes.updateAttributes(newTotalPrimaryAttributes);
    }

    //get weapon hashmap for external access
    public HashMap<Slot, Weapon> weapon(){
        return this.weapon;
    }
    
    // get equipment hashmap for external access
    public HashMap<Slot, Armor> allArmor(){
        return this.allArmor;
    }

    //equip an armor piece
    public boolean equipArmor(Armor armor) throws InvalidArmorException{
        if (!armor.validateItem(this)){
            throw new InvalidArmorException("Too high item level or invalid armor type to equip");
        }
        this.allArmor.put(armor.slot(), armor);
        this.updateTotalPrimaryAttrs();
        return true;
    }

    //validate if armor is appropriate type and level
    public boolean validateArmor(Armor armor){
        if (!this.allowedArmor.contains(armor.type())){
            return false;
        }
        if (this.level < armor.level()){
            return false;
        }
        return true;
    }

    //equip a weapon
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (!weapon.validateItem(this)){
            throw new InvalidWeaponException("Too high item level or invalid weapon type to equip");
        }
        this.weapon.put(weapon.slot(), weapon);
        return true;
    }

    //allowed weapons + armor getters
    public ArrayList allowedWeapons(){
        return this.allowedWeapons;
    }

    public ArrayList allowedArmor(){
        return this.allowedArmor;
    }

    //get all char stats
    public BasePrimaryAttributes basePrimaryAttributes(){
        return this.basePrimaryAttributes;
    }

    //total primary get
    public abstract int totalMainAttr();

    //ABSTRACT base primary attribute get
    public abstract int baseMainAttr();

    //name set and get
    public void setName(String name){
        this.name = name;
    }

    public String name(){
        return this.name;
    }

    //level get and levelUp
    public int level(){
        return this.level;
    }

    //base functionality of levelling up - overridden in child classes with more details
    public void levelUp(BasePrimaryAttributes extraPrimaryAttributes){
        this.level += 1;
        this.basePrimaryAttributes.updateAttributes(extraPrimaryAttributes);
    }

    //empty levelUp impl to allow overrides in child classes without parameter
    public void levelUp(){
    }

    //display character - later move to helper class
    @Override
    public String toString(){
        return this.printer.printCharacter(this);
    }
}

package rpg.character;

import rpg.attributes.BasePrimaryAttributes;
import rpg.constants.ArmorType;
import rpg.constants.WeaponType;

public class Rogue extends RpgCharacter{

    public Rogue(){
        super(2, 6, 1);
        this.allowedWeapons.add(WeaponType.DAGGER);
        this.allowedWeapons.add(WeaponType.SWORD);
        this.allowedArmor.add(ArmorType.LEATHER);
        this.allowedArmor.add(ArmorType.MAIL);
    }

    @Override
    public int totalMainAttr(){return this.totalPrimaryAttributes.dex();}

    @Override
    public int baseMainAttr(){
        return this.basePrimaryAttributes.dex();
    }

    @Override
    public void levelUp(){
        super.levelUp(new BasePrimaryAttributes(1, 4, 1));
        this.totalPrimaryAttributes.updateAttributes(new BasePrimaryAttributes(1, 4, 1));
    }

}

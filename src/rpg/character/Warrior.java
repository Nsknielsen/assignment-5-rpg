package rpg.character;

import rpg.attributes.BasePrimaryAttributes;
import rpg.constants.ArmorType;
import rpg.constants.WeaponType;

public class Warrior extends RpgCharacter{

    public Warrior(){
        super(5, 2, 1);
        this.allowedWeapons.add(WeaponType.AXE);
        this.allowedWeapons.add(WeaponType.HAMMER);
        this.allowedWeapons.add(WeaponType.SWORD);
        this.allowedArmor.add(ArmorType.MAIL);
        this.allowedArmor.add(ArmorType.PLATE);
    }

    @Override
    public int totalMainAttr(){return this.totalPrimaryAttributes.str();}

    @Override
    public int baseMainAttr(){
        return this.basePrimaryAttributes.str();
    }

    @Override
    public void levelUp(){
        super.levelUp(new BasePrimaryAttributes(3, 2, 1));
        this.totalPrimaryAttributes.updateAttributes(new BasePrimaryAttributes(3, 2, 1));
    }
}

package rpg.character;

import rpg.attributes.BasePrimaryAttributes;
import rpg.constants.ArmorType;
import rpg.constants.WeaponType;

public class Mage extends RpgCharacter{

    public Mage(){
        super(1, 1, 8);
        this.allowedWeapons.add(WeaponType.STAFF);
        this.allowedWeapons.add(WeaponType.WAND);
        this.allowedArmor.add(ArmorType.CLOTH);
    }

    @Override
    public int totalMainAttr(){return this.totalPrimaryAttributes.intel();}

    @Override
    public int baseMainAttr(){
        return this.basePrimaryAttributes.intel();
    }

    @Override
    public void levelUp(){
        super.levelUp(new BasePrimaryAttributes(1, 1, 5));
        this.updateTotalPrimaryAttrs(new BasePrimaryAttributes(1, 1, 5));
    }

}

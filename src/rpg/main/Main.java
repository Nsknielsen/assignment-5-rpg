package rpg.main;

import rpg.character.RpgCharacter;
import rpg.constants.WeaponType;
import rpg.contentGenerator.CharacterGenerator;
import rpg.contentGenerator.ItemGenerator;
import rpg.item.Armor;
import rpg.item.Weapon;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {


    public static void main(String[] args) throws Exception{

        CharacterGenerator characterGenerator = new CharacterGenerator();
        ItemGenerator itemGenerator = new ItemGenerator();
        ArrayList characterClassOptions = new ArrayList(Arrays.asList("ma", "ra", "ro", "wa"));

        //input reader
        InputStreamReader r = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(r);

        //get user input to generate a character
        System.out.println("Enter you character's name:");
        String characterName = reader.readLine();
        String characterClass = "";
        do {
            System.out.println("Pick a character class (Options: type ma for mage, ra for ranger, ro for rogue, wa for warrior)");
            characterClass = reader.readLine();
        } while (!characterClassOptions.contains(characterClass));
        RpgCharacter rpgCharacter = characterGenerator.createCharacter(characterName, characterClass);

        //get user input to pick a weapon to start with
        System.out.println("Pick a weapon (type the first 2 letters of the weapon name) - your options are :");
        for (Object weaponType : rpgCharacter.allowedWeapons()){
            System.out.println(weaponType);
        }
        String chosenWeaponType = reader.readLine();
        Weapon chosenWeapon = itemGenerator.createStarterWeapon(chosenWeaponType);
        rpgCharacter.equipWeapon(chosenWeapon);
        System.out.println(rpgCharacter.name() + " equipped the " + chosenWeapon.name());

        //get user input to pick a piece of armor to start with
        System.out.println("Time to pick some armor (type the frst 2 letters of the armor type) - your options are:");
        for (Object armorType: rpgCharacter.allowedArmor()){
            System.out.println(armorType);
        }
        String chosenArmorType = reader.readLine();
        System.out.println("Where do you want to wear it? (Type 'he' for head, 'bo' for body, or 'le' for legs)");
        String chosenArmorSlot = reader.readLine();
        Armor chosenArmor = itemGenerator.createStarterArmor(chosenArmorType, chosenArmorSlot);
        System.out.println(rpgCharacter.name() + " equipped the " + chosenArmor.name());

        //show final character
        System.out.println("*#* CHARACTER SHEET *#*");
        System.out.println(rpgCharacter.toString());
        System.out.println("Now go do cool stuff, " + rpgCharacter.name());


    }
}

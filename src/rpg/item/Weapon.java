package rpg.item;

import rpg.character.RpgCharacter;
import rpg.constants.WeaponType;

public class Weapon extends Item{

    private int damage;
    private double attackSpeed;
    private WeaponType type;

    //dps calculator
    public double calculateWeaponDps(){
        double weaponDps = this.damage * this.attackSpeed;
        return weaponDps;
    }

    //weapon validation for character class
    public boolean validateItem(RpgCharacter rpgCharacter){
        if (!rpgCharacter.allowedWeapons().contains(this.type)){
            return false;
        }
        if (rpgCharacter.level() < this.level()){
            return false;
        }
        return true;
    }

    //dmg get and set
    public void setDamage(int damage){
        this.damage = damage;
    }

    public int damage(){
        return this.damage;
    }

    //attack speed get and set
    public void setAttackSpeed(double attackSpeed){
        this.attackSpeed = attackSpeed;
    }

    public double attackSpeed(){
        return this.attackSpeed;
    }

    //type set and get
    public void setType(WeaponType type){
        this.type = type;
    }

    public WeaponType type(){
        return this.type;
    }


}

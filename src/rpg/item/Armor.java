package rpg.item;

import rpg.attributes.BasePrimaryAttributes;
import rpg.character.RpgCharacter;
import rpg.constants.ArmorType;

public class Armor extends Item{

    private ArmorType type;
    private BasePrimaryAttributes primaryAttributes;

    public Armor(){
    }

    //validate armor for character class
    public boolean validateItem(RpgCharacter rpgCharacter){
        if (!rpgCharacter.allowedArmor().contains(this.type)){
            return false;
        }
        if (rpgCharacter.level() < this.level()){
            return false;
        }
        return true;
    }

    //prim attr set and get
    public BasePrimaryAttributes primaryAttributes(){
        return this.primaryAttributes;
    }

    public void setPrimaryAttributes(BasePrimaryAttributes primaryAttributes){
        this.primaryAttributes = primaryAttributes;
    }

    //type set and get
    public ArmorType type(){
        return this.type;
    }

    public void setType(ArmorType type){
        this.type = type;
    }

}

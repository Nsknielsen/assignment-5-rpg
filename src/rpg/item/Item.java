package rpg.item;

import rpg.character.RpgCharacter;
import rpg.constants.Slot;

public abstract class Item{

    protected String name;
    protected int level;
    protected Slot slot;

    //validate item for character class
    public abstract boolean validateItem(RpgCharacter rpgCharacter);

    //name get and set
    public void setName(String name){
        this.name = name;
    }

    public String name(){
        return this.name;
    }

    //lvl requirement get and set
    public void setLevel(int level){
        this.level = level;
    }

    public int level(){
        return this.level;
    }

    //slot get and set
    public Slot slot(){
        return this.slot;
    }

    public void setSlot(Slot slot){
        this.slot = slot;
    }
}

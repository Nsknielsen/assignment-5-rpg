# DESCRIPTION
Backend project in Java intended to store and update RPG characters with various kinds of stats and equipment.
Allows the user to pick a name, class, and some starting equipment.

#INSTALLATION
Requires a Java Development Kit and an IDE similar to IntelliJ.

# HOW TO USE
Open the project in IntelliJ or similar IDE and run the Main class to try out an interactive character generator.
It's also fun to watch the test cases resolve.

# DEPENDENCIES
None.

#MAINTAINERS
Nikolaj Nielsen (@Nsknielsen)

